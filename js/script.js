const nameTabs = $('.tabs > li');

nameTabs.on('click', function() {
   const dataVal = $(this).data('tabContent');
    $('.tab-content').hide();
    $('.tabs-content').find(`[data-tab-content = ${dataVal}]`).show();

    $('.tabs-title.active').removeClass('active');
    $(this).toggleClass('active');
});